import { Component, OnInit } from '@angular/core';
import { backend } from '../server.service';
import { ActivatedRoute } from '@angular/router';
import { PageInfoServiceService } from '../page-info.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css'],
  providers: [ backend ]
})

export class CourseDetailComponent implements OnInit {

  coursesObj = {
    heading: "Kurs",
    description: "Info:",
    dataSourceLink: "https://my-courses-lab3-ance1901.herokuapp.com/api/courses/" + this.route.snapshot.params['id'],
    dataSourceName: "course"
  }

  course: any = {}

  constructor(private backend : backend, private route: ActivatedRoute, public pageInfo: PageInfoServiceService) {
    this.pageInfo.updateData(this.coursesObj);  // Update the observable in pageInfoServices. 
   }

  ngOnInit(): void {
    this.backend.getCourse(this.route.snapshot.params['id']).subscribe(course => {
      this.course = course[0];  
    });
  }
}