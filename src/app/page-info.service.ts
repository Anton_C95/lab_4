import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PageInfoServiceService {
  
  private pageInfoSource: any = new Subject<any>();
  pageInfoSource$ = this.pageInfoSource.asObservable();  // This observable will be subscribed to in the app.component

  // When the observable is updated it should emit message to the app.component which will assign the values in the
  // object to its own variables (description, title... etc)
  updateData(message: any) {  // my-courses, admin and courses component will call this function
    this.pageInfoSource.next(message);  // next == notify, pushes data  ** Unsure if this is emitting or if the 
    // issue lies with subscribing to the observable in app.component **
  }

  constructor() {}
}
