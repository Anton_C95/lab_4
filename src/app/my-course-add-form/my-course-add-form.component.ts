import { Component, OnInit } from '@angular/core';
import { backend } from '../server.service';

@Component({
  selector: 'app-my-course-add-form',
  templateUrl: './my-course-add-form.component.html',
  styleUrls: ['./my-course-add-form.component.css'],
  providers: [backend]
})
export class MyCourseAddFormComponent implements OnInit {
  courseCode: string = "";
  completed: boolean = false;
  courseCompleted: string = "";  // This is updated in the addCourse function depending on the completed boolean in the previous line
  formAmended: any;

  constructor(private backend: backend) { }

  ngOnInit(): void { }

  addCourse(form: any) {  
    if (form.completed == true) {  // Add the completed key value pair, the value depends on the checkbox value.
      this.courseCompleted = "avklarad";
    }
    else {
      this.courseCompleted = "oavklarad";
    }
    this.formAmended = {
      "courseCode": form.courseCode,
      "completed": this.courseCompleted
    }
    this.backend.addCourse(this.formAmended).subscribe(() => {
      console.log('added course')
      window.location.reload();  // Reload the page so that ngOnInit is run in admin.component to fetch the data. 
    }) 
  }
}
