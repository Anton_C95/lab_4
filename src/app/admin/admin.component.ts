import { Component, OnInit } from '@angular/core';
import { backend } from '../server.service';
import { PageInfoServiceService } from '../page-info.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  // Object with information to be sent to app.component for display
  adminObj = {
    heading: "Admin",
    description: "Administrera mina kurser:",
    dataSourceLink: "https://my-courses-lab3-ance1901.herokuapp.com/api/my/courses",
    dataSourceName: "my-courses"
  }

  myCourses: {completed: String, courseCode: String, institutionCode: String, level: String, 
    name: String, points: Number, progression: String, subject: String, subjectCode: String}[] = [];

  courseCode: string = ""

  constructor(private backend: backend, public pageInfo: PageInfoServiceService) {
    this.pageInfo.updateData(this.adminObj);  // When the page is loaded update the page-info.service. 
   }

  ngOnInit(): void {
    this.backend.getMyCourses().subscribe(myCourses => {
      this.myCourses = myCourses.myCourses;  // Select courses array from the object returned
    });
  }

  updateCompleted(courseCode: String) {  // Change the value of completed to true/false
    this.backend.updateCompleted(courseCode).subscribe(() => {
      window.location.reload();
    })
  }

  delete(courseCode: String) {  // Delete a course
    this.backend.deleteCourse(courseCode).subscribe(() => {
      window.location.reload();
    })
  }
}
