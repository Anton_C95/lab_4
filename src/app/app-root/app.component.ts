import { Component } from '@angular/core';
import { PageInfoServiceService } from '../page-info.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  // These variables will be updated by the observer declared in ngOnInit
  title = 'Kurser';
  description = 'Följande kurser finns i vårt kursutbud:'
  dataSourceLink = 'change me';
  dataSourceName = 'change me';

  constructor(public pageInfo: PageInfoServiceService) {}

  ngOnInit() {
    this.pageInfo.pageInfoSource$.subscribe((message: any) => {  // Listening to the page-info service.
      this.title = message.heading;
      this.description = message.description;
      this.dataSourceLink = message.dataSourceLink;
      this.dataSourceName = message.dataSourceName;
    })
  }
}
