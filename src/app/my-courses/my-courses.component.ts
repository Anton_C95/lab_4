import { Component, EventEmitter, Output } from '@angular/core';
import { PageInfoServiceService } from '../page-info.service';

@Component({
  selector: 'app-my-courses',
  templateUrl: './my-courses.component.html',
  styleUrls: ['./my-courses.component.css']
})
export class MyCoursesComponent {
  @Output() messageFromChild = new EventEmitter<any>();

  myCoursesObj = {
    heading: "Mina kurser",
    description: "Följande kurser har lästs eller läses för nuvarande:",
    dataSourceLink: "https://my-courses-lab3-ance1901.herokuapp.com/api/my/courses",
    dataSourceName: "my-courses"
  }

  filterString: string = "";

  public filterStringReceived(filterString: any) {
    this.filterString = filterString;
  }

  constructor(public pageInfo: PageInfoServiceService) {
    this.pageInfo.updateData(this.myCoursesObj); 
  }
}

