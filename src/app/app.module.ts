import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app-root/app.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { MyCoursesComponent } from './my-courses/my-courses.component';
import { MyCourseDetailComponent } from './my-course-detail/my-course-detail.component';
import { SubjectDetailComponent } from './subject-detail/subject-detail.component';
import { AdminComponent } from './admin/admin.component';
import { CourseFilterComponent } from './course-filter/course-filter.component';
import { CourseListComponent } from './course-list/course-list.component';
import { MyCourseListComponent } from './my-course-list/my-course-list.component';
import { MyCourseAddFormComponent } from './my-course-add-form/my-course-add-form.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PageInfoServiceService } from './page-info.service'; 

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    CourseDetailComponent,
    MyCoursesComponent,
    MyCourseDetailComponent,
    SubjectDetailComponent,
    AdminComponent,
    CourseFilterComponent,
    CourseListComponent,
    MyCourseListComponent,
    MyCourseAddFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [PageInfoServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
