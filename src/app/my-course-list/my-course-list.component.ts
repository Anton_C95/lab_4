import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { backend } from '../server.service';

@Component({
  selector: 'app-my-course-list',
  templateUrl: './my-course-list.component.html',
  styleUrls: ['./my-course-list.component.css']
})
export class MyCourseListComponent implements OnInit {

  // Input from my-courses which received value from course-filter
  @Input() filterString = "";

  myCourses: {
    completed: String, courseCode: String, institutionCode: String, level: String,
    name: String, points: Number, progression: String, subject: String, subjectCode: String
  }[] = [];

  filteredCourses: {
    completed: String, courseCode: String, institutionCode: String, level: String,
    name: String, points: Number, progression: String, subject: String, subjectCode: String
  }[] = [];

  constructor(private backend: backend) { }

  ngOnInit(): void {
    this.backend.getMyCourses().subscribe(myCourses => {
      this.myCourses = myCourses.myCourses;  // Select courses array from the object returned
      this.filteredCourses = this.myCourses;
    });
  }

  // Responds when Angular sets or resets data-bound input properties, in this case filterString which
  // is emitted on keyUp from the child course-filter.component.
  ngOnChanges() {
      this.filteredCourses = this.filterCourses();
      if (this.filterString == "") {
        this.filteredCourses = this.myCourses;
      }

  }

  filterCourses() {  // Filter the courses according the to the courseCode.
    return this.myCourses.filter(course => course.courseCode.toLowerCase().indexOf(this.filterString.toLowerCase()) !== -1) || 
    this.myCourses.filter(course => course.name.toLowerCase().indexOf(this.filterString.toLowerCase()) !== -1);  //  why doesn't this OR statement work? Cant filter on name
  } 

}
