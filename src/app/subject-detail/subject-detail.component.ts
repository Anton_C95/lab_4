import { Component, OnInit } from '@angular/core';
import { backend } from '../server.service';
import { ActivatedRoute } from '@angular/router';
import { PageInfoServiceService } from '../page-info.service';

@Component({
  selector: 'app-subject-detail',
  templateUrl: './subject-detail.component.html',
  styleUrls: ['./subject-detail.component.css'],
  providers: [ backend ]
})
export class SubjectDetailComponent implements OnInit {

  subjectObj = {
    heading: "Ämne",
    description: "Info:",
    dataSourceLink: "https://my-courses-lab3-ance1901.herokuapp.com/api/subjects/" + this.route.snapshot.params['id'],
    dataSourceName: "subject"
  }
  
  subject: any = {}

  constructor(private backend : backend, private route: ActivatedRoute, public pageInfo: PageInfoServiceService) { 
    this.pageInfo.updateData(this.subjectObj);  // Update the observable in pageInfoServices.
  }

  ngOnInit(): void { 
    this.backend.getSubject(this.route.snapshot.params['id']).subscribe(subject => {
      this.subject = subject.subjects[0];
    });
  }

}
