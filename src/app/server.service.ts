import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class backend {
  url = 'https://my-courses-lab3-ance1901.herokuapp.com/api/'

  constructor(private http: HttpClient) { }

  getCourses(): Observable<any> {
    return this.http.get(this.url + 'courses')
  }

  getCourse(id: any): Observable<any> {
    return this.http.get(this.url + 'courses/' + id);
  }

  getMyCourses(): Observable<any> {
    return this.http.get(this.url + 'my/courses');
  }

  getMyCourse(id: any): Observable<any> {
    return this.http.get(this.url + 'my/courses/' + id);
  }

  getSubject(id: any): Observable<any> {
    return this.http.get(this.url + 'subjects/' + id);
  }

  addCourse(form: any) {
    return this.http.post(this.url + 'my/courses/add', form);
  }

  deleteCourse(id: String) {
    return this.http.delete(this.url + 'my/courses/delete/' + id);
  }

  updateCompleted(id: String) {
    return this.http.put(this.url + 'my/courses/' + id, {}); 
  }
}