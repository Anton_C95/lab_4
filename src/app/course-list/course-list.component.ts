import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { backend } from '../server.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css'],
  providers: [backend]
})
export class CourseListComponent implements OnInit {

  // Input from my-courses which received value from course-filter
  @Input() filterString = "";

  courses: {
    courseCode: String, institutionCode: String, level: String,
    name: String, points: Number, progression: String, subject: String, subjectCode: String
  }[] = [];  // interface for the array, type checking purpose

  filteredCourses: {
    courseCode: String, institutionCode: String, level: String,
    name: String, points: Number, progression: String, subject: String, subjectCode: String
  }[] = [];

  constructor(private backend: backend) { }

  ngOnInit(): void {
    this.backend.getCourses().subscribe(courses => {
      this.courses = courses.courses;  // Select courses array from the object returned
      this.filteredCourses = this.courses;  // When the page first loads assign filteredCourses all courses
    });
  }

  // Responds when Angular sets or resets data-bound input properties, in this case filterString which
  // is emitted on keyUp from the child course-filter.component.
  ngOnChanges() {
    this.filteredCourses = this.filterCourses();  
    if (this.filterString == "") {
      this.filteredCourses = this.courses;
    }

  }

  // Returns an array filtered according to the filterstring. 
  filterCourses() {
    return (this.courses.filter(course => course.courseCode.toLowerCase().indexOf(this.filterString.toLowerCase()) !== -1) ||
      this.courses.filter(course => course.name.toLowerCase().indexOf(this.filterString.toLowerCase()) !== -1));
  }

}
