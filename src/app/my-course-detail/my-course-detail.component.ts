import { Component, OnInit } from '@angular/core';
import { backend } from '../server.service';
import { ActivatedRoute } from '@angular/router';
import { PageInfoServiceService } from '../page-info.service';


@Component({
  selector: 'app-my-course-detail',
  templateUrl: './my-course-detail.component.html',
  styleUrls: ['./my-course-detail.component.css'],
  providers: [ backend ]
})
export class MyCourseDetailComponent implements OnInit {

  myCourseObj = {
    heading: "Min kurs",
    description: "Info:",
    dataSourceLink: "https://my-courses-lab3-ance1901.herokuapp.com/api/my/courses/" + this.route.snapshot.params['id'],
    dataSourceName: "my course"
  }


  myCourse: any = {}  // Could go on to define the object for better type safety however this is tedious.
  constructor(private backend : backend, private route: ActivatedRoute, public pageInfo: PageInfoServiceService) { 
    this.pageInfo.updateData(this.myCourseObj);  // Update the observable in pageInfoServices.
  }

  ngOnInit(): void { 
    this.backend.getMyCourse(this.route.snapshot.params['id']).subscribe(myCourse => {
      this.myCourse = myCourse[0];  
    });
  }
}
