import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { CoursesComponent } from './courses/courses.component';
import { MyCoursesComponent } from './my-courses/my-courses.component';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { MyCourseDetailComponent } from './my-course-detail/my-course-detail.component';
import { SubjectDetailComponent } from './subject-detail/subject-detail.component';


const routes: Routes = [
  {
    path: '',
    component: CoursesComponent
  },
  {
    path: 'courses',
    redirectTo: ''
  },
  {
    path: 'courses/:id',
    component: CourseDetailComponent
  },
  {
    path: 'my-courses/:id',
    component: MyCourseDetailComponent
  },
  {
    path: 'subjects/:id',
    component: SubjectDetailComponent
  },
  {
    path: 'my-courses',
    component: MyCoursesComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}