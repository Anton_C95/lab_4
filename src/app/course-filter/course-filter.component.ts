import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-course-filter',
  templateUrl: './course-filter.component.html',
  styleUrls: ['./course-filter.component.css']
})
export class CourseFilterComponent implements OnInit {

  @Output() messageFilter = new EventEmitter();

  // When this function is called an event is emitted which is captured by my-courses.component
  public sendFilterString(filterString: string) {
    this.messageFilter.emit(filterString);
  }

  constructor() {}

  ngOnInit(): void {}

}



/// https://www.youtube.com/watch?v=IvASSPQMrUE&ab_channel=kudvenkat
