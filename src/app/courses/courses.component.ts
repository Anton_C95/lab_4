import { Component } from '@angular/core';
import { PageInfoServiceService } from '../page-info.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent {

  filterString: string = "";

  coursesObj = {
    heading: "Kurser",
    description: "Följande kurser finns i vårt kursutbud:",
    dataSourceLink: "https://my-courses-lab3-ance1901.herokuapp.com/api/courses",
    dataSourceName: "courses"
  }

  public filterStringReceived(filterString: any) {
    this.filterString = filterString;
  }

  constructor (public pageInfo: PageInfoServiceService) {
    this.pageInfo.updateData(this.coursesObj); 
  }
}
